<?php

namespace App\Http\Controllers;
use \App\transaksi;
use DB;
use Illuminate\Http\Request;

class ipaymuController extends Controller
{
    public function singleProduct(){
        $product = DB::table('produk')->first();
        // print $product;

        $array = array('product[]' => $product->nama,
        'qty[]' => '1',
        'price[]' => $product->harga,
        'description[]' => $product->deskripsi,
        'returnUrl' => 'http://127.0.0.1:8000/',
        'notifyUrl' => 'http://127.0.0.1:8000/',
            'cancelUrl' => 'http://127.0.0.1:8000',
            'referenceId' => 'ID1234',
            'weight[]' => '1',
            'dimension[]' => '1:1:1',
            'buyerName' => 'nama pembeli',
            'buyerEmail' => 'email@mail.com',
            'buyerPhone' => '08123456789',
            'pickupArea' => '80117',
            'pickupAddress' => 'Jakarta');
            $data = http_build_query($array);
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://sandbox.ipaymu.com/api/v2/payment',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $data,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'signature: [object Object]',
    'va: 1179000899',
    'timestamp: 20191209155701'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
 


    }
}
